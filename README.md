# Apperta Principles

This is a site that describes software and generic principles that guide projects supported by the Apperta Foundation.

The `master` branch of this project is deliberately kept separate from the `pages` branch that creates the site deployed at https://apperta.gitlab.io/principles//#

